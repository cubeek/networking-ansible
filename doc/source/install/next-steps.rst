.. _next-steps:

Next steps
~~~~~~~~~~

If you have installed and configured Networking-Ansible as an ML2 driver, your
OpenStack environment now includes the networking-ansible ML2 Driver.

To add additional services, see https://docs.openstack.org/.

If you plan to use Networking-Ansible as a Python API the module can now be
imported and instantiated. Please note the API has been deprecated and will
be removed in the next release. Please use
https://github.com/ansible-network/network-runner instead of the
Networking-Ansible API. See :ref:`api` for usage.
